using System;
using System.Linq;

namespace InputsIntoArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[0];
            int numb;
            int a = 1;
            //
            while (a != 11)
            {
                Console.WriteLine("Enter number {0}...\n", a);
                numb = Convert.ToInt16(Console.ReadLine());
                array = array.Concat(new int[] { numb }).ToArray();//<--uses .Linq
                a = a + 1; Console.Clear();
            }

            var ascend = array.OrderBy(i => i);

            Console.WriteLine("\n\n");
            foreach (var item in ascend)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
