using System;

namespace CalculatorTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            Decimal numa;
            Decimal numb;
            string action;
            Decimal output = 0;

            Console.WriteLine("enter num1");
            numa = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("enter num2");
            numb = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("enter an action ie x + - or /");
            action = Console.ReadLine();

            if (action == "x" || action == "*")
                {output = numa * numb;}
            if (action == "+")
                {output = numa + numb;}
            if (action == "-")
                {output = numa - numb;}
            if (action == "/")
                {output = numa / numb;}

            Console.Write("answer was {0}", output);
        }
    }
}
