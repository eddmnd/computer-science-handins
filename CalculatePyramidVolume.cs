using System;

namespace CalculatorTest1Fork
{
    class Program
    {
        static void Main(string[] args)
        {
            Decimal basew;
            Decimal basel;
            Decimal height;
            Decimal output = 0;

            Console.WriteLine("enter pyramid base width in x units");
            basew = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("enter pyramid base length in the same units");
            basel = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("enter pyramid height in the same units");
            height = Convert.ToDecimal(Console.ReadLine());

            output = (basew * basel * height)/3;

            Console.Write("answer was {0} in x units", output);
        }
    }
}
