using System;

namespace hangme
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter word");
            string word = Console.ReadLine().ToLower();
            string masked = word.Substring(word.Length).PadLeft(word.Length, '*');
            int tries = word.Length + 2;
            //
            Console.Clear();
            while (masked.Contains("*") && tries != 0)
            {
                Console.WriteLine("Enter guess");
                char guess = Console.ReadKey().KeyChar;
                string strguess = Convert.ToString(guess);
                int index = word.IndexOf(guess);

                int count = word.Length - word.Replace(strguess, "").Length;

                if (!word.Contains(guess))
                {
                    Console.WriteLine("incorrect guess");
                    tries = tries - 1;
                }
                else
                {
                    while (count != 0)
                    {
                        word = word.Remove(index, 1);
                        word = word.Insert(index, " ");
                        masked = masked.Remove(index, 1);
                        masked = masked.Insert(index, strguess);
                        index = word.IndexOf(guess);
                        count = count - 1;
                    }
                }
                Console.Clear(); Console.WriteLine("{0}\ntries left:{1}\n", masked, tries);
                //Console.WriteLine(word);
            }
            if (tries == 0)
            {
                Console.WriteLine("You Lose with these letters remaining:\n{0}", word);
            }
            else
            {
                Console.WriteLine("You Win with {0} tries remaining", tries);
            }
            Console.WriteLine("\nquitting in...");
            int timer = 3; while (timer != 0)
            {
                Console.WriteLine(timer); timer = timer - 1;
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
