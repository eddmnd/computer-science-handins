using System;
using System.Collections.Generic;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            Char Sea = '*';
            int ypos = 0;
            int xpos = 0;
            Char[,] theBoard = new Char[10, 20];
            int linecount = 0;
            int yguess;
            int xguess;

            // generate random
            Random r = new Random();
            int ranx = r.Next(17);
            int rany = r.Next(9);
            List<int> listy = new List<int>();
            listy.Add(rany);
            List<int> listx = new List<int>();
            listx.Add(ranx);
            listx.Add(ranx + 1);
            listx.Add(ranx + 2);

            // fill array
            for (xpos = 0; xpos < 10; xpos++)
            {
                for (ypos = 0; ypos < 20; ypos++)
                {
                    theBoard[xpos, ypos] = Sea;
                }
            }

            //loop
            int i = 0;
            while (i < 3)
            {
                foreach (int q in listx)
                {
                    Console.WriteLine("x:\n{0}", q);
                }
                foreach (int c in listy)
                {
                    Console.WriteLine("y:\n{0}", c);
                }

                System.Threading.Thread.Sleep(1000);

                for (xpos = 0; xpos < 10; xpos++)
                {
                    Console.SetCursorPosition(0, linecount);
                    for (ypos = 0; ypos < 20; ypos++)
                    {
                        Console.Write("{0}", theBoard[xpos, ypos]);
                    }
                    linecount++;
                }
                linecount = 0;
                //Enter x and y guesses
                Console.WriteLine("enter spaces from the top");
                yguess = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("enter spaces from the left (+2) coord");
                xguess = Convert.ToInt32(Console.ReadLine());

                //test for y,x in theBoard
                bool isInyList = listy.IndexOf(yguess) != -1;
                if (isInyList is false)
                {
                    Console.WriteLine("miss");
                    theBoard[yguess, xguess] = 'M';
                }
                else
                {
                    bool isInxList = listx.IndexOf(xguess) != -1;
                    if (isInxList is false)
                    {
                        Console.WriteLine("miss");
                        theBoard[yguess, xguess] = 'M';
                    }
                    else
                    {
                        Console.WriteLine("hit");
                        theBoard[yguess, xguess] = 'H';
                        i++;
                    }
                }
                System.Threading.Thread.Sleep(1000);
                Console.Clear();
                //if y,x is hit:    theBoard[y, x] = 'H'; 
            }
            Console.Clear();
            Console.WriteLine("you win");
        }
    }
}
