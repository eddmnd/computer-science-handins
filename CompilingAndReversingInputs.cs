using System;
using System.Collections.Generic;
using System.Linq;

namespace CompilingAndReversingInputs
{
    class Program
    {
        static void Main(string[] args)
        {
            string a;
            bool loop = true;
            //
            List<string> list = new List<string>();

            while (loop == true)
            {
                Console.WriteLine("enter a character and press enter.\npress # to stop.\n");
                a = Console.ReadLine();
                if (a == "#") { break; }
                else { list.Add(a); }
                Console.Clear();
            }

            list.Reverse();
            foreach (string item in list)
            {
                Console.Write(item.ToString());
            }


        }
    }
}
