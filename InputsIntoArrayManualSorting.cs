using System;

namespace InputsIntoArrayManualSorting
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[10];
            int count = 0;
            int a; int temp;
            int x; int y;
            //
            while (count != 10)
            {
                Console.WriteLine("enter number {0}", count + 1);
                a = Convert.ToInt16(Console.ReadLine());
                array[count] = a;
                count = count + 1; Console.Clear();
            }
            foreach (int item in array)
            {
                Console.WriteLine(item);
            }
            //manual sorting
            y = 1; x = 0;
            while (x != 10)
            {
                while (y != 10)
                {
                    if (array[x] < array[y])
                    {
                        temp = array[x];
                        array[x] = array[y];
                        array[y] = temp;
                    }
                    y = y + 1;
                }
                x = x + 1;
                y = x + 1;
            }
            Console.WriteLine("\n");
            foreach (int item in array)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}