using System;
using System.Collections.Generic;//<--used for creating lists
using System.Linq;//<-- used for sorting lists

namespace RainfallVisualisation
{
    class Program
    {
        static void Main(string[] args)
        {
            int month = 1;
            int rain;
            string masked;
            List<int> list = new List<int>();
            List<string> strlist = new List<string>();
            //
            while (month != 13)
            {
                Console.WriteLine("enter rain value for month {0} in mm...", month);

                rain = Convert.ToInt16(Console.ReadLine());
                list.Add(rain);

                masked = new string('*', rain);
                strlist.Add(masked);

                month = month + 1; Console.Clear();
            }

            month = 1;
            foreach (string item in strlist)
            {
                if (month < 10) {Console.WriteLine("month " + month + ")  " + item.ToString());}
                else {Console.WriteLine("month " + month + ") " + item.ToString());}
                month = month + 1;
            }

            Console.WriteLine("\nmaximum) " + list.Max() + "mm");
            Console.WriteLine("minimum) " + list.Min() + "mm");
            Console.WriteLine("total)   " + list.Sum() + "mm");
            Console.WriteLine("average) " + list.Average() + "mm"); 
        }
    }
}