using System;
using System.Collections.Generic;

namespace OrderingInputs
{
    class Program
    {
        protected static void Main(string[] args)
        {
            int hsnum = 1;
            int hsval1 = 0;
            int hsval2 = 0;
            int hsval3 = 0;
            //
            while (hsnum != 4)
            {
                Console.WriteLine("enter the value of heat sensor {0}...", hsnum);
                if (hsnum == 1) { hsval1 = Convert.ToInt16(Console.ReadLine()); }
                if (hsnum == 2) { hsval2 = Convert.ToInt16(Console.ReadLine()); }
                if (hsnum == 3) { hsval3 = Convert.ToInt16(Console.ReadLine()); }
                hsnum = hsnum + 1; Console.Clear();
            }

            Console.WriteLine("heat sensor 1 value: {0}\n" +
                              "heat sensor 2 value: {1}\n" +
                              "heat sensor 3 value: {2}\n"
                              , hsval1, hsval2, hsval3);

            if (hsval1 < 17) { Console.WriteLine("room 1 heating turned on"); }
            if (hsval2 < 17) { Console.WriteLine("room 2 heating turned on"); }
            if (hsval3 < 17) { Console.WriteLine("room 3 heating turned on"); }

            Console.WriteLine("\nthe value of the sensors in descending order is:");
            List<int> list = new List<int>();//<--using .Collections.Generic
            list.Add(hsval1);
            list.Add(hsval2);
            list.Add(hsval3);

            list.Sort();
            list.Reverse();

            foreach (int item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
