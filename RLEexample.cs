using System;
using System.Collections.Generic;
using System.IO;

namespace RLEexample
{
    class Program
    {
        static void Main(string[] args)
        {
            //GLOBAL VAR DEC//
            int option = 0;
            string path = "DataFile1";
            string FileData;
            Int64 towrite = 0;
            var CompList = new List<string>();
            //SWITCH LOOP//
            while (option != 9)
            {
                //OPTIONS//
                Console.Clear();
                Console.WriteLine("\tOptions:\n\n1) Write to DataFile1" +
                                  "\n2) Read DataFile1\n3) Compress DataFile1" +
                                  "\n\n9) Quit\n");
                option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        {
                            Console.Clear();
                            Console.Write("Enter String of Numbers: ");
                            towrite = Convert.ToInt64(Console.ReadLine());
                            using (StreamWriter sw = File.CreateText(path))
                            {
                                sw.WriteLine(towrite);
                            }
                            break;
                        }
                    case 2:
                        {
                            Console.Clear();
                            FileData = File.ReadAllText(path);
                            Console.Write(FileData);
                            Console.ReadLine(); break;
                        }
                    case 3:
                        {
                            Console.Clear();FileData = File.ReadAllText(path);
                            int index = 0;while (index != FileData.Length -1)
                            {
                                char extracted = Convert.ToChar(FileData.Substring(index, 1));
                                char tocompare = Convert.ToChar(FileData.Substring(index + 1, 1));
                                int amnt = 1;int temp = 1;
                                while (extracted == tocompare)
                                {
                                    amnt++; temp++;
                                    tocompare = Convert.ToChar(FileData.Substring(index + temp, 1));
                                }
                                CompList.Add(amnt.ToString());
                                CompList.Add(extracted.ToString());index = index + amnt;
                            }
                            int i = 0;foreach (string item in CompList)
                            {
                                i++;
                                if (i % 2 == 0) { Console.Write("({0}), ", item); }
                                else { Console.Write(item); }
                            }
                            Console.ReadLine();CompList.Clear();break;
                        }
                }
            }
        }
    }
}
