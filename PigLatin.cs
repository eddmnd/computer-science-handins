using System;
using System.Linq;

namespace PigLatin
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = Console.ReadLine();
            int index;
            int count;
            string store;
            //
            sentence = sentence.Insert(sentence.Length, " ");
            count = sentence.Count(" ".Contains);//<--uses LINQ
            while (count != 0)
            {
                index = sentence.IndexOf(' ');
                sentence = sentence.Remove(index, 1);
                sentence = sentence.Insert(index, "ay_");
                count = count - 1;
            }
            sentence = sentence.Insert(0, "_");
            sentence = sentence.Remove(sentence.Length - 1);
            //^-- stops the program thinking there is a word at the end when there is none
            /*Console.WriteLine(sentence);*/
            count = sentence.Count("_".Contains);//<--uses LINQ
            while (count != 0)
            {
                index = sentence.IndexOf('_');
                sentence = sentence.Remove(index, 1);
                sentence = sentence.Insert(index, " ");
                index = index + 1;
                store = sentence.Remove(0, index);
                store = store.Remove(1);
                /*Console.WriteLine(store);
                Console.WriteLine(sentence);*/
                sentence = sentence.Remove(index, 1);
                if (count == 1)//<-- stops program missing last insertion
                {
                    sentence = sentence.Insert(sentence.Length, "_");
                }
                index = sentence.IndexOf('_');
                index = index - 2;
                sentence = sentence.Insert(index, store);
                count = count - 1;
            }
            sentence = sentence.Remove(0, 1);
            sentence = sentence.Remove(sentence.Length - 1);
            Console.WriteLine(sentence);
        }
    }
}