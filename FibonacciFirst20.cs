using System;

namespace FibonacciFirst20
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int b = 1;
            int next;
            int loop = 0;
            //
            Console.Write(a + " ");
            Console.Write(b + " ");
            while (loop != 18)
            {
                next = a + b;
                Console.Write(next + " ");
                a = b;
                b = next;
                loop = loop + 1;
            }
        }
    }
}
