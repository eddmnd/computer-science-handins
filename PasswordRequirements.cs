using System;
using System.Linq;

namespace PasswordRequirements
{
    class Program
    {
        static void Main(string[] args)
        {
            string password;
            int loop = 3;
            while (loop != 0)
            {
                Console.WriteLine("password must contain capital(s), number(s)," +
                                  " and be 8 or more characters long\n");
                Console.WriteLine("enter password..."); password = Console.ReadLine();
                loop = 3; Console.Clear();
                if (password.Length > 8) { loop = loop - 1; }
                if (password.Any(char.IsUpper)) { loop = loop - 1; }
                if (password.Any(char.IsDigit)) { loop = loop - 1; }
                if (loop != 0)
                {
                    Console.WriteLine("try again...");
                }
            }
            Console.WriteLine("password accepted");
        }
    }
}
