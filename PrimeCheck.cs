using System;

namespace PrimeCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            int numb;
            bool prime = true;
            int i;
            //
            Console.WriteLine("enter a number...");
            numb = Convert.ToInt16(Console.ReadLine());
            if (numb == 1 || numb == 0)
            {
                prime = false;
            }
            else if (numb == 2) { Console.WriteLine("2 is a prime number"); prime = true; }
            else
            {
                for (i = 2; i < numb; i++)
                {
                    if (numb % i == 0) { prime = false; }// numb % 1 gives remainder of numb / i
                }
            }
            if (prime)
            {
                Console.WriteLine("{0} is prime", numb);
            }
            else { Console.WriteLine("{0} is not prime", numb); }
        }
    }
}