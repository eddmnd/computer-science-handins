using System;

namespace HangManFinal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter word");
            //
            string word = Convert.ToString(Console.ReadLine()); Console.Clear();
            string masked = word.Substring(word.Length).PadLeft(word.Length, '_');
            string guess = "";
            var guesses = word.Length + 2;
            //
            while (word.Length != 0 && guesses != 0)
            {
                Console.WriteLine(masked);
                Console.WriteLine("guesses left: {0}", guesses);
                Console.WriteLine("letters left: {0}", word.Length);
               
                bool loop = true; while (loop == true)
                {
                    guess = Convert.ToString(Console.ReadLine());
                    if (guess.Length == 0)
                    {
                        loop = true;
                    }
                    else
                    {
                        loop = false;
                    }
                }
            
                if (word.Contains(guess))
                {
                    Console.WriteLine("{0} is in the word\n\n", guess);
                    word = word.Replace(guess, "");

                    //int index = word.IndexOf(guess); //<<FIX THIS | if we know where guess lies in word, we can place guess in the same place into masked
                    //Console.WriteLine(index);

                    masked = masked.Remove(0, 1); // replace 0 with index when above works
                    masked = masked.Insert(0, guess);
                    //shorten mask by cutting off end
                    //masked = masked.Substring(0, word.Length);
                }
                else
                {
                    guesses = guesses - 1;
                }
                Console.ReadLine(); Console.Clear();
            }
            Console.Clear(); if (guesses == 0)
            {
                Console.WriteLine("you lost with {0} characters remaining", word.Length);
            }
            else
            {
                Console.WriteLine("you win with {0} guesses remaining", guesses);
            }
            Console.Write("quitting in...\t");
            int timer = 3; while (timer != 0)
            {
                Console.Write(timer); timer = timer - 1;
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
