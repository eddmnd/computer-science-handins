using System;

namespace hangme
{
    class Program
    {
        static void Main(string[] args)
        {



            string word = Console.ReadLine();
            string masked = word.Substring(word.Length).PadLeft(word.Length, '*');


            //while (word.Length != 1)
            while (masked.Contains("*"))
            {

                char guess = Console.ReadKey().KeyChar;
                string strguess = Convert.ToString(guess);



                int index = word.IndexOf(guess);

                word = word.Replace(guess, ' ');
                //word = word.Replace(guess, 'z');
                //masked = masked.Remove(0, 1); //cut off start of string
                masked = masked.Remove(index, 1);
                masked = masked.Insert(index, strguess); //insert

                Console.WriteLine("{0}\n{1}\n{2}", index, word, masked);
            }


        }
    }
}
/*using System;

namespace hangme
{
    class Program
    {
        static void Main(string[] args)
        {
            string word = Console.ReadLine();
            string masked = word.Substring(word.Length).PadLeft(word.Length, '*');
            char guess = Console.ReadKey().KeyChar;
            string strguess = Convert.ToString(guess);
            int index;


            index = word.IndexOf(guess);
            word = word.Replace(guess, 'z');
            //masked = masked.Substring(index);
            masked = masked.Remove(0, 1); //cut off start of string
            masked = masked.Insert(index, strguess);

            Console.WriteLine("{0}\n{1}\n{2}", index, word, masked);


            //word = word.Insert(guess, "");
            //test = test.Substring(0, 5);
        }
    }
}*/
