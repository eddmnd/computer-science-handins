using System;

namespace StringManipulationMenu
{
    class Program
    {
        static void Main(string[] args)
        {
            string word;
            string guess;
            int choice;
            bool loop = true;
            char[] array;
            //
            Console.WriteLine("enter a word...");
            word = Console.ReadLine().ToLower();
            Console.Clear();

            while (loop == true)
            {
                Console.Clear();
                Console.WriteLine("1) reverse the word");
                Console.WriteLine("2) search the word for a letter");
                Console.WriteLine("3) enter a new word");
                Console.WriteLine("9) quit");
                Console.WriteLine("\nchoose an option...");
                choice = Convert.ToInt16(Console.ReadLine());
                if (choice == 1)
                {
                    Console.Clear();
                    array = word.ToCharArray();
                    Array.Reverse(array);
                    Console.WriteLine(array);
                    Console.ReadLine();
                }
                else if (choice == 2)
                {
                    Console.Clear();
                    Console.WriteLine("enter a search term...");
                    guess = Console.ReadLine().ToLower();
                    if (word.Contains(guess))
                    {
                        Console.WriteLine("{0} was found in your word {1}"
                                          , guess, word);
                    }
                    else
                    {
                        Console.WriteLine("{0} was not found in your word {1}"
                                          , guess, word);
                    }
                    Console.ReadLine();
                }
                else if (choice == 3)
                {
                    Console.Clear();
                    Console.WriteLine("enter a new word...");
                    word = Console.ReadLine();
                }
                else if (choice == 9) { loop = false; }
                else
                {
                    Console.Clear();
                    Console.WriteLine("invalid input...");
                    Console.ReadLine();
                }
            }
        }
    }
}
